#!/bin/bash
while IFS= read -r line; do 
  IP=$(echo $line | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}');
  if [ -n "$IP" ]; then 
    sshpass -p $PASSWORD ssh-copy-id -p $PORT $USER@$IP; 
  fi; 
  done < hosts
